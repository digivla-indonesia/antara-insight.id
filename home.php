<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Digivla</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <!--<script src="js/bootstrap.min.js"></script>

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
    function initialize() {
        var myLatlng = new google.maps.LatLng(-6.290866, 106.810523);
        var mapOptions = {zoom: 15, center: myLatlng}
        var map = new google.maps.Map(document.getElementById("map-container1"), mapOptions);
        var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: "the manhattan square"
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <script>
    function initialize() {
        var myLatlng = new google.maps.LatLng(-6.236908, 106.812653);
        var mapOptions = {zoom: 15, center: myLatlng}
        var map = new google.maps.Map(document.getElementById("map-container2"), mapOptions);
        var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: "jl cikatomas II No.19"
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    </script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-lg-3">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top"><img class="img-brand" src="img/logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="col-lg-7 center-nav collapse navbar-collapse hints" id="bs-example-navbar-collapse-1">
                <ul style="margin-top: 10px;" class="menu nav navbar-nav col-lg-12">
                    <li>
                        <a class="page-scroll" href="#page-top">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#service">Service</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#client">Client</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact Us</a>
                    </li>
                    <li>
                        <a class="page-scroll last" href="coba_gratis.php">Free Trial</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-2 sosial">
                <ul class="nav navbar-nav navbar-right hiding">
                    <li>
                        <a class="page-scroll" href="#about"><i style="text-decoration:underline;color:#fff;" class="fa fa-facebook fa-fw"></i></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services"><i style="text-decoration:underline;color:#fff;" class="fa fa-linkedin fa-fw"></i></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="layer">
            <div class="header-content">
                <div class="header-content-inner">
                    <img src="img/img-top.png" id="img1">
                    <h5>DISCOVER MORE</h5>                
                    <img src="img/w-icon.png" class="img-logo">
                </div>
            </div>
            <div id="wlc">
                <div class="container">
                <div class="top-more">
                    <a href="" class="btn btn-default btn-xl">Welcome to Digivla</a>
                    <p>Here’s the place where people are really serious in turning news into actionable insight. Digivla is an integrated media monitoring provider where evolving technology is enriched by an embedded process of digitalizing 200+ print, electronic and online media, both national and local. In addition, a dedicated team of communication consultants from Digivla’s affiliated company, PT. Royston Advisory Indonesia is assigned to translate the data into useful information and further convert it to come up with actionable insight in a form of public relations recommendations. In Digivla, we believe that the primary strength of an insight is the quality of the team that works behind the information.</p>
                    <div class="brand-logo">
                        <h3 class="h3-logo">THE RATIONALE</h3>              
                        <img src="img/w-icon.png" class="img-logo">
                    </div>
                </div>
                </div>
            </div>  
        </div>
        
    </header>
    <section class="no-padding" id="">
        <div class="container-fluid">
            <div class="row no-gutter layer1">
                <div class="col-lg-4 col-sm-12 col-md-4 hg1 gr3">
                    <a href="" data-toggle="modal" data-target="#myModal1">
                        <div class="show-news">
                            <div class="col-xs-4">
                                <img src="img/n3.png">
                            </div>
                            <div class="col-xs-8">
                                <h3>Because media has a case to be believed</h3>
                                <p>By nature, there is no way we could control the media to silent the skeptic minors in voicing bad things about us. But yet there is a way for us to monitor them, spot the trend and observe the movement of certain issues impacting us. By using our media monitoring solution, you will be able to collect useful information and gain actionable insight to pre-empt issues, ignore or react to them.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-4 hg1 gr2">
                    <a href="" class="" data-toggle="modal" data-target="#myModal2">
                        <div class="show-news">
                            <div class="col-xs-4">
                                <img src="img/n2.png">
                            </div>
                            <div class="col-xs-8">
                                <h3>Because an issue ignored is a crisis ensured</h3>
                                <p>There are lots of reasons why a number of industries is now under so much scrutiny in Indonesia. One is the raised of key stakeholders expectation under current political dynamic, which is fueled by the growth of both mass and social media. Digivla's proprietary feature allows its users to monitor movement of specific issues which is starting from potential, emerging, current to crisis. Not only that, our team of consultants would provide actionable recommendation in the time of crisis. All in one package.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-4 hg1 gr1">
                    <a href="" class="" data-toggle="modal" data-target="#myModal3">
                        <div class="show-news">
                            <div class="col-xs-4">
                                <img src="img/n1.png">
                            </div>
                            <div class="col-xs-8">
                                <h3>Because the best way to navigate the news is to give up all your controls</h3>
                                <p>The growth of media is undoubtedly exponential in Indonesia. There are 139 million Indonesians who are connected to the internet, in which 93,6% of them have the access to social media and online news portals. In 2012 only, there are over 94 million copies of 1,324 print media were circulated nationwide annually. There are also 776 press companies registered to the National Press Council. Interestingly, according to Trust Barometer Survey conducted by Edelman, media is the most trusted institution by Indonesian public for three years in a row (2012-2014), in contrast to the goverment, private sector and NGO's</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!--MODAL-->
        <!-- Modal -->
        <div id="myModal3" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content hg1 layer-abu">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><img src="img/close1.png"/></button>
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                      <img src="img/n1.png" class="img-ver">  
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <h3>Because the best way to navigate the news is to give up all your controls</h3>
                        <p>The growth of media is undoubtedly exponential in Indonesia. There are 139 million Indonesians who are connected to the internet, in which 93,6% of them have the access to social media and online news portals. In 2012 only, there are over 94 million copies of 1,324 print media were circulated nationwide annually. There are also 776 press companies registered to the National Press Council. Interestingly, according to Trust Barometer Survey conducted by Edelman, media is the most trusted institution by Indonesian public for three years in a row (2012-2014), in contrast to the goverment, private sector and NGO's.</p>
                    </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div id="myModal2" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content hg1 layer-abu">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><img src="img/close1.png"/></button>
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                      <img src="img/n2.png" class="img-ver">  
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <h3>Because an issue ignored is a crisis ensured</h3>
                        <p>There are lots of reasons why a number of industries is now under so much security in Indonesia. One is the raised of key stakeholders expectation under current political dynamic, which is fueled by the growth of both mass and social media. Digivla's proprietary feature allows its users to monitor movement of specific issues which is starting from potential, emerging, current to crisis. Not only that, our team of consultants would provide actionable recommendation in the time of crisis. All in one package.</p>
                    </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div id="myModal1" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content hg1 layer-abu">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><img src="img/close1.png"/></button>
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                      <img src="img/n3.png" class="img-ver">  
                    </div>
                    <div class="col-xs-12 col-md-9">
                        <h3>Because media has a case to be believed</h3>
                        <p>By nature, there is no way we could control the media to silent the skeptic minors in voicing bad things about us. But yet there is a way for us to monitor them, spot the trend and observe the movement of certain issues impacting us. By using our media monitoring solution, you will be able to collect useful information and gain actionable insight to pre-empt issues, ignore or react to them.</p>
                    </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        
        <!--MODAL-->
    </section>
    <div class="brand-logo">
        <h3 class="h3-logo">THE PROCESS</h3>              
        <img src="img/w-icon.png" class="img-logo">
    </div>        
    <section class="no-padding" id="" style="margin-bottom:50px;">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-12 col-md-4">
                    <div class="portfolio-box">
                        <img src="img/p3.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption gr2">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    NEWS
                                </div>
                                <div class="project-image col-xs-6 col-md-4">
                                    <img src="img/news.png">
                                </div>
                                <div class="project-name col-xs-6 col-md-8">
                                    <p>236 media<br/>
                                    86 national and local print media<br/>
                                    14 national TV stations<br/>
                                    10 network radio stations<br/>
                                    52 national news portal<br/>
                                    74 local news portal</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-4">
                    <div class="portfolio-box">
                        <img src="img/p2.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption gr3">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Information
                                </div>
                                <div class="project-image col-xs-6 col-md-4">
                                    <img src="img/information.png">
                                </div>
                                <div class="project-name col-xs-6 col-md-8">
                                    <p>available backtrack data since 2010 print, tv and radio news monitoring in one dashboard unlimited search keyword & validated news tonality downloadable media information dashboard</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-4">
                    <div class="portfolio-box folio">
                        <img src="img/p1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption gr2">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Insight
                                </div>
                                <div class="project-image col-xs-6 col-md-4">
                                    <img src="img/insight.png">
                                </div>
                                <div class="project-name col-xs-6 col-md-8">
                                    <p>news tonality validation with accountable SOP tailored news contect daily report and summary media content analysis & early warning system issue life cycle and crisis alert actionable public relations recommendation.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  <section class="no-padding" id="service" style="padding-top:20px;margin-bottom:50px;">
    <div class="brand-logo">
        <h3 class="h3-logo">OUR SERVICES</h3>              
        <img src="img/w-icon.png" class="img-logo">
    </div>
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-12 col-md-4 gr1">
                    <a href="#" class="portfolio-box folio" data-toggle="modal" data-target="#sc3">
                        <img src="img/baseline.jpg" class="img-responsive center-img" alt="">
                        <div class="gr1 portfolio-box-caption">
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-4 gr2">
                    <a href="#" class="portfolio-box folio" data-toggle="modal" data-target="#sc2">
                        <img src="img/validate.jpg" class="img-responsive center-img" alt="">
                        <div class="gr2 portfolio-box-caption">
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-4 gr3">
                    <a href="#" class="portfolio-box folio" data-toggle="modal" data-target="#sc1">
                        <img src="img/media-insight.jpg" class="img-responsive center-img" alt="">
                        <div class="gr3 portfolio-box-caption">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="sc1" class="modal fade" role="dialog">
          <div class="modal-dialog dialog-big">
            <div class="modal-content layer">
              <div class="modal-body">
                <div class="row">
                    <div class="container">
                        <button type="button" class="close white" data-dismiss="modal"><img src="img/close2.png"/></button>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                      <img src="img/media-insight.png">  
                    </div>
                    <div class="col-md-3">
                        <h3>Scope Of Service</h3>
                        <ul class="list-center br-3">
                            <li class="li1">Baseline Keywords<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li2">Daily Media Monitoring Report & Summary<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li1">Weekly Media Statistics Report & Summary<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li2">Monthly Media Content Analysis Report<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li1">Crisis Alert & PR Recomendation<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li2">Monthly Client Site's Visitation<span><i class="fa fa-check fa-2x"></i></span></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Featured & Data Coverage</h3>
                        <ul class="list-right br-3">
                            <li class="li0"><p><span class="left">Number of search keyword setup</span><span class="right">Unlimited</span></p></li>
                            <li class="li0"><p><span class="left">Print Media Monitoring Coverage</span><span class="right">86 Media</span></p></li>
                            <li class="li0"><p><span class="left">News Portal Monitoring Coverage</span><span class="right">149 Media</span></p></li>
                            <li class="li0"><p><span class="left">Television Monitoring Coverage</span><span class="right">14 Stations</span></p></li>
                            <li class="li0"><p><span class="left">Radio Monitoring Coverage</span><span class="right">10 Stations</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : National Mainstream Dailies</span><span class="right">07.30 AM</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : Local Print Dailies</span><span class="right">H+1</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : Magazines & Tabloids</span><span class="right">H+7</span></p></li>
                            <li class="li0"><p><span class="left">Latest Submission Time for Daily Report</span><span class="right">09.00 AM</span></p></li>
                            <li class="li0"><p><span class="left">Latest Completion Time for Tone Validation</span><span class="right">08.00 AM</span></p></li>
                            <li class="li0"><p><span class="left">News clipping format (automated)</span><span class="right">MS Word, PDF, TXT, JPEG</span></p></li>
                            <li class="li0"><p><span class="left">Media monitoring dashboard download format</span><span class="right">Ms Word, PPT, Excel</span></p></li>
                            <li class="li0"><p><span class="left">Default perid for search featre</span><span class="right">1 Years backtrack</span></p></li>
                            <li class="li0"><p><span class="left">Push E-Mail Feature</span><span class="right">Operated by user</span></p></li>
                            <li class="li0"><p><span class="left">Mobile version view</span><span class="right">Yes, mobile-friendly web format</span></p></li>
                            <li class="li0"><p><span class="left">Maximum viewer dedicated account</span><span class="right">15 Accounts</span></p></li>
                            <li class="li0"><p><span class="left">Maximm additional media</span><span class="right">5 print national media, 10 online news</span></p></li>
                            <li class="li0"><p><span class="left">Additional backtrack data upon request</span><span class="right">From 2010</span></p></li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="sc2" class="modal fade" role="dialog">
          <div class="modal-dialog dialog-big">
            <div class="modal-content layer">
              <div class="modal-body">
                <div class="row">
                    <div class="container">
                        <button type="button" class="close white" data-dismiss="modal"><img src="img/close2.png"/></button>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                      <img src="img/validate.png">  
                    </div>
                    <div class="col-md-3">
                        <h3>Scope Of Service</h3>
                        <ul class="list-center br-2">
                            <li class="li1">Baseline Keywords<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li2">Daily Media Monitoring Report & Summary<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li1">Weekly Media Statistics Report & Summary<span><i class="fa fa-close fa-2x"></i></span></li>
                            <li class="li2">Monthly Media Content Analysis Report<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li1">Crisis Alert & PR Recomendation<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li2">Monthly Client Site's Visitation<span><i class="fa fa-check fa-2x"></i></span></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Featured & Data Coverage</h3>
                        <ul class="list-right br-2">
                            <li class="li0"><p><span class="left">Number of search keyword setup</span><span class="right">Unlimited</span></p></li>
                            <li class="li0"><p><span class="left">Print Media Monitoring Coverage</span><span class="right">86 Media</span></p></li>
                            <li class="li0"><p><span class="left">News Portal Monitoring Coverage</span><span class="right">149 Media</span></p></li>
                            <li class="li0"><p><span class="left">Television Monitoring Coverage</span><span class="right">14 Stations</span></p></li>
                            <li class="li0"><p><span class="left">Radio Monitoring Coverage</span><span class="right">10 Stations</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : National Mainstream Dailies</span><span class="right">07.30 AM</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : Local Print Dailies</span><span class="right">H+1</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : Magazines & Tabloids</span><span class="right">H+7</span></p></li>
                            <li class="li0"><p><span class="left">Latest Submission Time for Daily Report</span><span class="right">11.30 AM</span></p></li>
                            <li class="li0"><p><span class="left">Latest Completion Time for Tone Validation</span><span class="right">09.00 AM</span></p></li>
                            <li class="li0"><p><span class="left">News clipping format (automated)</span><span class="right">MS Word, PDF, TXT, JPEG</span></p></li>
                            <li class="li0"><p><span class="left">Media monitoring dashboard download format</span><span class="right">Ms Word, PPT, Excel</span></p></li>
                            <li class="li0"><p><span class="left">Default perid for search featre</span><span class="right">1 Years backtrack</span></p></li>
                            <li class="li0"><p><span class="left">Push E-Mail Feature</span><span class="right">Operated by user</span></p></li>
                            <li class="li0"><p><span class="left">Mobile version view</span><span class="right">Yes, mobile-friendly web format</span></p></li>
                            <li class="li0"><p><span class="left">Maximum viewer dedicated account</span><span class="right">10 Accounts</span></p></li>
                            <li class="li0"><p><span class="left">Maximm additional media</span><span class="right">5 print national media, 10 online news</span></p></li>
                            <li class="li0"><p><span class="left">Additional backtrack data upon request</span><span class="right">From 2013</span></p></li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="sc3" class="modal fade" role="dialog">
          <div class="modal-dialog dialog-big">
            <div class="modal-content layer">
              <div class="modal-body">
                <div class="row">
                    <div class="container">
                        <button type="button" class="close white" data-dismiss="modal"><img src="img/close2.png"/></button>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                      <img src="img/baseline.png">  
                    </div>
                    <div class="col-md-3">
                        <h3>Scope Of Service</h3>
                        <ul class="list-center br-1">
                            <li class="li1">Baseline Keywords<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li2">Daily Media Monitoring Report & Summary<span><i class="fa fa-close fa-2x"></i></span></li>
                            <li class="li1">Weekly Media Statistics Report & Summary<span><i class="fa fa-close fa-2x"></i></span></li>
                            <li class="li2">Monthly Media Content Analysis Report<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li1">Crisis Alert & PR Recomendation*<span><i class="fa fa-check fa-2x"></i></span></li>
                            <li class="li2">Monthly Client Site's Visitation**<span><i class="fa fa-check fa-2x"></i></span></li>
                        </ul>
                        <p style="color: white; font-size: 10px; font-style: italic;"><sup style="font-size: 8px;">*)</sup>Yes, if tonality validated daily by user</p>
                        <p style="color: white; font-size: 10px; font-style: italic; margin-top: -15px;"><sup style="font-size: 8px;">**)</sup>Yes, upon client's request / call</p>
                    </div>
                    <div class="col-md-4">
                        <h3>Featured & Data Coverage</h3>
                        <ul class="list-right br-1">
                            <li class="li0"><p><span class="left">Number of search keyword setup</span><span class="right">Unlimited</span></p></li>
                            <li class="li0"><p><span class="left">Print Media Monitoring Coverage</span><span class="right">86 Media</span></p></li>
                            <li class="li0"><p><span class="left">News Portal Monitoring Coverage</span><span class="right">149 Media</span></p></li>
                            <li class="li0"><p><span class="left">Television Monitoring Coverage</span><span class="right">14 Stations</span></p></li>
                            <li class="li0"><p><span class="left">Radio Monitoring Coverage</span><span class="right">10 Stations</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : National Mainstream Dailies</span><span class="right">07.30 AM</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : Local Print Dailies</span><span class="right">H+1</span></p></li>
                            <li class="li0"><p><span class="left">Latest Upload Time : Magazines & Tabloids</span><span class="right">H+7</span></p></li>
                            <!-- <li class="li0"><p><span class="left">Latest Submission Time for Daily Report</span><span class="right">11.30 AM</span></p></li> -->
                            <!-- <li class="li0"><p><span class="left">Latest Completion Time for Tone Validation</span><span class="right">09.00 AM</span></p></li> -->
                            <li class="li0"><p><span class="left">News clipping format (automated)</span><span class="right">MS Word, PDF, TXT, JPEG</span></p></li>
                            <li class="li0"><p><span class="left">Media monitoring dashboard download format</span><span class="right">Ms Word, PPT, Excel</span></p></li>
                            <li class="li0"><p><span class="left">Default perid for search featre</span><span class="right">1 Years backtrack</span></p></li>
                            <li class="li0"><p><span class="left">Push E-Mail Feature</span><span class="right">Operated by user</span></p></li>
                            <li class="li0"><p><span class="left">Mobile version view</span><span class="right">Yes, mobile-friendly web format</span></p></li>
                            <li class="li0"><p><span class="left">Maximum viewer dedicated account</span><span class="right">5 Accounts</span></p></li></li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <section id="client" style="padding-top:20px;">
        <div class="brand-logo">
            <h3 class="h3-logo">PORTOFOLIO</h3>              
            <img src="img/w-icon.png" class="img-logo">
        </div>
        <div class="container">
            <div class="row">
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s1.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s2.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s3.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s4.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s5.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s6.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s7.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s8.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s9.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s10.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s11.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s12.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s13.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s14.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s15.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s16.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s17.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s18.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s19.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s20.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s21.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s22.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s23.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s24.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s25.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s26.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s27.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s28.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s29.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s30.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s31.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s32.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s33.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s34.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s35.png">
                    </div>
                    <div class="col-sm-1 col-xs-4 pass">
                        <img class="img-responsive center-img" src="img/s36.png">
                    </div>
                </div>
            </div>             
        </div>
    </section>
    <section id="contact" style="padding-top:15px;">
        <div class="brand-logo">
            <h3 class="h3-logo">CONTACT US</h3>              
            <img src="img/w-icon.png" class="img-logo">
        </div>
        <div class="container" style="margin-bottom:20px;">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <img src="img/of1.png" class="img-responsive">
                </div>
                <div class="col-lg-5 col-md-5">
                    <div id="map-container1" style="height:200px;"></div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <p>
                        <b>OFFICE :</b>
                        <br/>THE MANHATTAN SQUARE, 15th FLOOR
                        <br/>JL. TB SIMATUPANG KAV. 1S,
                        <br/>JAKARTA 12549 INDONESIA
                        <br/>TELP: +6221 29407234 - FAX: +6221 29407325
                    </p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <img src="img/of2.png" class="img-responsive">
                </div>
                <div class="col-lg-5 col-md-5">
                    <div id="map-container2" style="height:200px;"></div>
                </div>
                <div class="col-lg-5 col-md-5">
                    <p>
                        <b>PRODUCTION :</b>
                        <br/>JL. CIKATOMAS II NO.19,
                        <br/>JAKARTA 12081 INDONESIA
                        <br/>TELP: +6221 7245081 - FAX: +6221 7245082
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="brand-logo"><a href="#">
        <h3 class="h3-logo">CLICK FOR<br>FREE TRIAL</h3>              
        <img src="img/w-icon.png" class="img-logo"></a>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/creative.js"></script>

</body>

</html>
