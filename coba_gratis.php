<!DOCTYPE html>
<html lang=en>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">

	    <title>Digivla</title>

	    <!-- Bootstrap Core CSS -->
	    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
	    <script type="text/javascript" src="js/jquery.js"></script>
	    <script src="js/bootstrap.min.js"></script>

	    <!-- Custom Fonts -->
	    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,700' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

	    <!-- Custom CSS -->
	    <link rel="stylesheet" href="css/creative.css" type="text/css">
	</head>

	<body id="page-top">
		<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
	        <div class="container-fluid">
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header col-lg-3">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand page-scroll" href="#page-top"><img class="img-brand" src="img/logo.png"></a>
	            </div>

	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="col-lg-7 center-nav collapse navbar-collapse hints" id="bs-example-navbar-collapse-1">
	                <ul style="margin-top: 10px;" class="menu nav navbar-nav col-lg-12">
	                    <li>
	                        <a class="page-scroll" href="index.php#page-top">Home</a>
	                    </li>
	                    <li>
	                        <a class="page-scroll" href="index.php#service">Service</a>
	                    </li>
	                    <li>
	                        <a class="page-scroll" href="index.php#client">Client</a>
	                    </li>
	                    <li>
	                        <a class="page-scroll" href="index.php#contact">Contact Us</a>
	                    </li>
	                    <li>
	                        <a class="page-scroll last" href="coba_gratis.php">Free Trial</a>
	                    </li>
	                </ul>
	            </div>
	            <div class="col-lg-2 sosial">
	                <ul class="nav navbar-nav navbar-right hiding">
	                    <li>
	                        <a class="page-scroll" href="index.php#about"><i style="text-decoration:underline;color:#fff;" class="fa fa-facebook fa-fw"></i></a>
	                    </li>
	                    <li>
	                        <a class="page-scroll" href="index.php#services"><i style="text-decoration:underline;color:#fff;" class="fa fa-linkedin fa-fw"></i></a>
	                    </li>
	                </ul>
	            </div>
	            <!-- /.navbar-collapse -->
	        </div>
	        <!-- /.container-fluid -->
	    </nav>

	    <section class="panel panel-default" id="service" style="padding-top:69px;margin-bottom:50px;">
	    	<!-- -->
	    	<legend class="panel-heading" style="color: #222;">Formulir Free Trial</legend>
	    	<form class="form-horizontal panel-body">
				<fieldset>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="textinput">Company / Organization Name</label>  
				  <div class="col-md-5">
				  <input id="company" name="company" type="text" placeholder="Company Name" class="form-control input-md">
				  </div>
				</div>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="sector">Sector / Industry</label>
				  <div class="col-md-5">
				    <select id="sector" name="sector" class="form-control">
				      <option value="Government">Government</option>
				      <option value="State Owned Enterprise">State Owned Enterprise</option>
				      <option value="Finance">Finance</option>
				      <option value="Retail / FMCG">Retail / FMCG</option>
				      <option value="Energy">Energy</option>
				      <option value="Telecommunication / IT">Telecommunication / IT</option>
				      <option value="Property / Infrastructure">Property / Infrastructure</option>
				      <option value="Transportation / Logistic">Transportation / Logistic</option>
				      <option value="Infrastructure">Infrastructure</option>
				      <option value="Maritime">Maritime</option>
				      <option value="Non-profit Organizations">Non-profit Organizations</option>
				      <option value="Others">Others</option>
				    </select>
				  </div>
				</div>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="textinput">Specific keywords to be included in our search engine</label>  
				  <div class="col-md-5">
				  <input id="company2" name="company2" type="text" placeholder="Keuangan" class="form-control input-md">
				  </div>
				</div>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="textinput">Contact Person</label>  
				  <div class="col-md-5">
				  <input id="fullname" name="fullname" type="text" placeholder="i.e. John Doe" class="form-control input-md">
				  </div>
				</div>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="textinput">Email Address</label>  
				  <div class="col-md-5">
				  <input id="personal_email" name="personal_email" type="email" placeholder="john@doe.com" class="form-control input-md">
				  </div>
				</div>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="textinput">Phone / Mobile</label>  
				  <div class="col-md-5">
				  <input id="handphone" name="handphone" type="text" placeholder="088888888" class="form-control input-md">
				  </div>
				</div>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="address">Text Area</label>
				  <div class="col-md-4">                     
				    <textarea class="form-control" id="address" name="address"></textarea>
				  </div>
				</div>

				<div class="form-group">
				  <label class="col-md-4 control-label" for="submit-btn"></label>
				  <div class="col-md-4">                     
				    <button type="submit" class="btn btn-info" id="submit-btn">Submit</button>
				  </div>
				</div>

				</fieldset>
			</form>
	    	<!-- -->
	    </section>

	</body>
</html>